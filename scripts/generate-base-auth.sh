#!/bin/bash

if [ $# -eq 2 ]
  then
    htpasswd -c ../generated/$1.htpasswd $2
  else
    echo "Invalid arguments. Usage: ./generate-base-auth.sh FILE_NAME USERNAME"
fi
